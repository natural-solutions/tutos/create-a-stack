#!/bin/bash

set -e 

cd ./docker/keycloak/scripts
zip -r ../keycloak-scripts.jar .
